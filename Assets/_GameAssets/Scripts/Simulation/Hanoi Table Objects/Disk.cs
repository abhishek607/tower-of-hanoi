﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disk : MonoBehaviour
{
    DiskPicker diskPicker => DiskPicker.instance;
    ObjectHighlighter highlighter => ObjectHighlighter.instance;
    public float DiskSize => transform.localScale.x;

    [SerializeField] GameObject CurrentMaterialObject;
    public Pole Pole;
    public int IndexInPole;

    public bool CanPickUp()
    {
        return (Pole != null && IndexInPole == 0);
    }

    public void OnDiskClick()
    {
        if (!CanPickUp())
        {
            GameController.instance.ShowError("Can only pick the top Disk");
            return;
        }
        // check if we can highlight this object if so then highlight it.
        if (diskPicker.CanPickObject(gameObject))
        {
            SoundPlayer.PlayDiskPickSound();
            diskPicker.PickObject(gameObject);
            Pole.RemoveDisk();
        }
    }

    public void OnDiskHighlight()
    {
        // check if we can highlight this object if so then highlight it.
        if (GameHelper.CanHiglight(gameObject.tag))
        {
            highlighter.HighlightObject(CurrentMaterialObject);
        }
        else
        {
            // it we can't highlight the current object then Clear all highlights.
            highlighter.ClearHighlighted();
        }
    }
}
