﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pole : MonoBehaviour
{
    DiskPicker diskPicker => DiskPicker.instance;
    ObjectHighlighter highlighter => ObjectHighlighter.instance;

    [SerializeField] List<Disk> Disks = new List<Disk>();
    [SerializeField] bool IsFinalPole;

    private void OnEnable()
    {
        GameEvents.OnPerformedOneMove += OnPerformedOneMove;
    }

    private void OnDisable()
    {
        GameEvents.OnPerformedOneMove -= OnPerformedOneMove;
    }

    private void Start()
    {
        RefreshDiskValues();
    }

    // Add a disk in the stack.
    public void AddDisk(Disk disk)
    {
        Disks.Add(disk);
        disk.transform.eulerAngles = new Vector3(0, 0, 0);
        disk.transform.SetParent(transform);
        disk.transform.localPosition = new Vector3(0, NextDiskHeight(), 0);

        RefreshDiskValues();
    }

    // Remove the top disk from the stack.
    public void RemoveDisk()
    {
        if (Disks.Count > 0)
        {
            Disks.RemoveAt(Disks.Count - 1);
        }

        RefreshDiskValues();
    }

    void RefreshDiskValues()
    {
        int DiskIndex = 0;
        for (int i = Disks.Count - 1; i >= 0; i--)
        {
            Disks[i].Pole = this;

            Disks[i].IndexInPole = DiskIndex;
            DiskIndex++;
        }
    }

    public void OnPoleClick()
    {
        if (diskPicker.lastPickedObject != null)
        {
            SoundPlayer.PlayDiskDropSound();

            AddDisk(diskPicker.lastPickedObject.GetComponent<Disk>());
            diskPicker.LeaveObject();

            // Register this as one move and call the event.
            SessionDataController.instance.Moves++;
            GameEvents.OnPerformedOneMove?.Invoke();
        }
    }

    public void OnPoleHighlight()
    {
        // check if we can highlight this object if so then highlight it.
        if (GameHelper.CanHiglight(gameObject.tag))
        {
            highlighter.HighlightObject(gameObject);
        }
        else
        {
            // it we can't highlight the current object then Clear all highlights.
            highlighter.ClearHighlighted();
        }
    }

    float NextDiskHeight()
    {
        float BottomHeight = -0.45f;
        float DiskHeightOffset = (Disks.Count - 1) * 0.2f;
        return BottomHeight + DiskHeightOffset;
    }

    void OnPerformedOneMove()
    {
        // Check if user has successfully completed Tower of Hanoi.
        if (IsFinalPole && Disks.Count > 0)
        {
            if (GameController.instance == null) return;

            // If i have all the disks in my column
            if(GameController.instance.AllDisks.Count == Disks.Count)
            {
                float LastDiskSize = 100;
                foreach (var item in Disks)
                {
                    if (item.DiskSize < LastDiskSize) {
                        LastDiskSize = item.DiskSize;
                    }
                    else
                    {
                        // Disks are not arranged in the right order.
                        return;
                    }
                }

                // All disks were arranged in decreasing size.
                GameEvents.OnSimulationComplete?.Invoke();
            }
        }
    }
}
