﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RaycastReceiver : MonoBehaviour
{
    public UnityEvent OnRaycastHit;
    public UnityEvent OnLeftClickOnObject;

    public void AtRaycastHit()
    {
        OnRaycastHit?.Invoke();
    }

    public void AtLeftClick()
    {
        OnLeftClickOnObject?.Invoke();
    }
}
