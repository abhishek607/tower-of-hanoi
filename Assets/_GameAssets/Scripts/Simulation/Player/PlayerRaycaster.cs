﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class performs all the raycasts needed to highlight and pick the right disks.
/// </summary>
public class PlayerRaycaster : MonoBehaviour
{

    ObjectHighlighter highlighter => ObjectHighlighter.instance;
    [SerializeField] Transform Crosshair;
    [SerializeField] Camera RaycastCamera;

    void ClickObjectInFrontOfCam()
    {
        float rayDistance = 1000.0f;
        // Ray from the center of the viewport.
        Ray ray = new Ray(RaycastCamera.transform.position, RaycastCamera.transform.forward);
        Debug.DrawRay(RaycastCamera.transform.position, RaycastCamera.transform.forward * 1000, Color.green);

        RaycastHit rayHit;
        // Check if we hit something.
        if (Physics.Raycast(ray, out rayHit, rayDistance, GameHelper.RaycastLayerMask()))
        {
            // Get the object that was hit.
            GameObject hitObject = rayHit.collider.gameObject;
            // Check and call the Click event on the receiver
            if (hitObject.GetComponent<RaycastReceiver>() != null)
                hitObject.GetComponent<RaycastReceiver>().AtLeftClick();
        }
    }

    void HighlightObjectInCenterOfCam()
    {
        float rayDistance = 1000.0f;
        // Ray from the center of the viewport.
        Ray ray = new Ray(RaycastCamera.transform.position, RaycastCamera.transform.forward);
        Debug.DrawRay(RaycastCamera.transform.position, RaycastCamera.transform.forward * 1000, Color.green);
        //Crosshair.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));

        RaycastHit rayHit;
        // Check if we hit something.
        if (Physics.Raycast(ray, out rayHit, rayDistance, GameHelper.RaycastLayerMask()))
        {
            // Get the object that was hit.
            GameObject hitObject = rayHit.collider.gameObject;
            // Check and call the highlight event on the receiver
            if (hitObject.GetComponent<RaycastReceiver>() != null)
                hitObject.GetComponent<RaycastReceiver>().AtRaycastHit();
            else
                highlighter.ClearHighlighted();
        }
        else
        {
            // it there is no raycast result then Clear all highlights.
            highlighter.ClearHighlighted();
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && DiskPicker.instance != null)
        {
            ClickObjectInFrontOfCam();
        }
        else if(ObjectHighlighter.instance != null)
        {
            HighlightObjectInCenterOfCam();
        }     
    }
}
