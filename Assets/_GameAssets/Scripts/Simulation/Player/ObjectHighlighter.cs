﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerRaycaster))]
public class ObjectHighlighter : MonoBehaviour
{
    public static ObjectHighlighter instance;

    public Material highlightMaterial;
    Material originalMaterial;
    GameObject lastHighlightedObject;

    private void Awake()
    {
        instance = this;
    }

    public void HighlightObject(GameObject gameObject)
    {
        if (lastHighlightedObject != gameObject)
        {
            ClearHighlighted();
            
            originalMaterial = gameObject.GetComponent<MeshRenderer>().sharedMaterial;
            highlightMaterial.SetColor("Color_C6174FD2", originalMaterial.color);
            gameObject.GetComponent<MeshRenderer>().sharedMaterial = highlightMaterial;
            lastHighlightedObject = gameObject;
        }

    }

    public void ClearHighlighted()
    {
        if (lastHighlightedObject != null)
        {
            lastHighlightedObject.GetComponent<MeshRenderer>().sharedMaterial = originalMaterial;
            lastHighlightedObject = null;
        }
    }
}
