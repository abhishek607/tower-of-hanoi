﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to pick the disk.
/// </summary>
[RequireComponent(typeof(PlayerRaycaster))]
public class DiskPicker : MonoBehaviour
{
    public static DiskPicker instance;

    [SerializeField] Transform PickedObjectParent;
    public GameObject lastPickedObject;

    private void Awake()
    {
        instance = this;
    }

    public void PickObject(GameObject gameObject)
    {
        if (lastPickedObject == null)
        {
            lastPickedObject = gameObject;
            lastPickedObject.transform.SetParent(PickedObjectParent);
            lastPickedObject.transform.localPosition = new Vector3(0, 0, 0);
            lastPickedObject.GetComponent<MeshCollider>().enabled = false;

            GameHelper.HoldingDisk = true;
        }
    }

    public void LeaveObject()
    {
        lastPickedObject.GetComponent<MeshCollider>().enabled = true;
        lastPickedObject = null;
        GameHelper.HoldingDisk = false;
    }

    public bool CanPickObject(GameObject objectToPick)
    {
        return lastPickedObject == null && GameHelper.CanPickObject(objectToPick.tag);
    }
}
