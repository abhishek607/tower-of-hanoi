﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class holds all the static data we need to check for game's current state.
public static class GameHelper 
{
    private static bool isPaused;
    public static bool IsPaused
    {
        get { return isPaused; }
        set
        {
            isPaused = value;

            Cursor.visible = value;
            Cursor.lockState = value? CursorLockMode.None: CursorLockMode.Locked;
        }
    }
    public static bool HoldingDisk;

    /// <summary>
    /// Checks if we can highlight the passed tag or not.
    /// </summary>
    public static bool CanHiglight(string tag)
    {
        if (IsPaused) return false;
        //Debug.Log("Checking Can Highlight for " + tag + " Object Holding " + HoldingDisk);
        if (tag.Equals(GameTags.Disk))
        {
            // Allow highlighting disks if user is not already holding a disk in his hands.
            return !HoldingDisk;
        }else if (tag.Equals(GameTags.Pole))
        {
            // Allow higlighting poles if user is holding disk in his hands.
            return HoldingDisk;
        }else
        {
            // Don't highlight anything else.
            return false;
        }
    }

    public static bool CanPickObject(string tag)
    {
        if (IsPaused) return false;

        return tag.Equals(GameTags.Disk);
    }

    public static int RaycastLayerMask()
    {
        // Ignore layer 8 from the raycasts.
        int layerMask = 1 << 8;
        layerMask = ~layerMask;
        return layerMask;
    }
}
