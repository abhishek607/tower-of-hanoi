﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

// This class controls all the UI Panels, and shows right panels at the right event.
public class GameController : MonoBehaviour
{
    public static GameController instance;
    [SerializeField] GameObject MenuObjects, SimulationObject;
    [SerializeField] GameObject SimulationCompletePanel, PausePanel;

    [Header("ErrorPanel")]
    [Space(10)]
    [SerializeField] GameObject ErrorPanel;
    [SerializeField] TextMeshProUGUI ErrorText;
    private Coroutine ErrorShowRoutine;
    private CanvasGroup ErrorPanelCanvasGroup => ErrorPanel.GetComponent<CanvasGroup>();


    [Space(10)]
    public List<Disk> AllDisks = new List<Disk>();
    
    private void Awake()
    {
        instance = this;

        // Set the game state as paused.
        GameHelper.IsPaused = true;
        
        MenuObjects.SetActive(true);
        SimulationObject.SetActive(false);
        ErrorPanel.SetActive(false);
    }

    private void OnEnable()
    {
        GameEvents.OnSimulationStart += OnSimulationStart;
        GameEvents.OnSimulationComplete += OnSimulationComplete;
    }

    private void OnDisable()
    {
        GameEvents.OnSimulationStart -= OnSimulationStart;
        GameEvents.OnSimulationComplete -= OnSimulationComplete;
    }

    public void OnStartGameClick()
    {
        if (!Cursor.visible) return;

        SoundPlayer.PlayClickSound();
        GameEvents.OnSimulationStart?.Invoke();
    }

    public void OnRestartClick()
    {
        if (!Cursor.visible) return;

        SoundPlayer.PlayClickSound();
        SceneLoader.ReloadScene();

        GameEvents.OnSimulationStart?.Invoke();
    }

    public void OnQuitClick()
    {
        if (!Cursor.visible) return;

        SoundPlayer.PlayClickSound();
        MenuObjects.SetActive(true);
        SimulationObject.SetActive(false);
    }

    // On Simulation start Show the Gameplay panels, and Hide Menu Panels.
    void OnSimulationStart()
    {
        Logger.Log("Starting Simulation", LogColor.Green, LogType.General);
        MenuObjects.SetActive(false);
        SimulationObject.SetActive(true);

        GameHelper.IsPaused = false;
    }

    // On Simulation complete call to show the Simulation Complete panel.
    void OnSimulationComplete()
    {
        GameHelper.IsPaused = true;
        SoundPlayer.PlayLevelCompleteSound();
        SimulationCompletePanel.gameObject.SetActive(true);
    }

    #region Error Pop Up

    public void ShowError(string error)
    {
        if(ErrorShowRoutine != null)
        {
            StopCoroutine(ErrorShowRoutine);
            DOTween.KillAll();
            ErrorPanel.gameObject.SetActive(false);
            ErrorPanelCanvasGroup.alpha = 1;
        }

        ErrorText.text = error;
        ErrorShowRoutine = StartCoroutine(ShowErrorIEnum());
    }

    IEnumerator ShowErrorIEnum()
    {
        ErrorPanelCanvasGroup.alpha = 1;
        ErrorPanel.transform.localScale = new Vector3(1, 1, 1);
        ErrorPanel.gameObject.SetActive(true);
        ErrorPanel.transform.DOPunchScale(new Vector3(0.1f, 0.1f, 0), 0.5f);

        yield return new WaitForSeconds(1f);
        ErrorPanelCanvasGroup.DOFade(0, 1f);
        yield return new WaitForSeconds(1f);
        ErrorPanel.SetActive(false);
    }

    #endregion

    #region Pause

    // Keep checking for Esc press and Pause game if its detected.
    private void Update()
    {
        if (!GameHelper.IsPaused && Input.GetKeyDown(KeyCode.Escape))
        {
            OnPause();
        }
    }

    // Pause the game
    public void OnPause()
    {
        SoundPlayer.PlayClickSound();

        GameHelper.IsPaused = true;
        PausePanel.SetActive(true);
    }

    // Resume the game
    public void OnResume()
    {
        SoundPlayer.PlayClickSound();

        GameHelper.IsPaused = false;
        PausePanel.SetActive(false);
    }

    #endregion
}
