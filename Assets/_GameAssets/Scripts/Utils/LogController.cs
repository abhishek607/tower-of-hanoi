﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LogColor
{
    Red,
    Yellow,
    Blue,
    Cyan,
    Green,
    White,
    Grey
}

public enum LogType
{
    General,
    Testing,
}

public static class Logger
{
    public static void Log(string message, LogColor logColor = LogColor.Grey, LogType logType = LogType.General)
    {
        Debug.Log("<color=" + logColor.ToString() + "> " +  message + "</color>");
    }
}
