﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameTags 
{
    public const string Pole = "Pole";
    public const string Disk = "Disk";
}
