﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneLoader
{
    public static bool IsLoadingScene;

    public static void ReloadScene()
    {
        IsLoadingScene = true;
        var operation = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
        operation.completed += SceneLoaded;
    }

    public static void LoadScene(string SceneName)
    {
        IsLoadingScene = true;
        var operation = SceneManager.LoadSceneAsync(SceneName);
        operation.completed += SceneLoaded;
    }

    private static void SceneLoaded(AsyncOperation obj)
    {
        IsLoadingScene = false;
    }

    public static void UnloadScene(string SceneName)
    {
        SceneManager.UnloadSceneAsync(SceneName);
    }
}
