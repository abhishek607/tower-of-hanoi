﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// Shows the animations of the player and other player tile on the bottom.
/// </summary>
public class ButtonAnimation : MonoBehaviour
{
    bool isInitialised = false;
    private Vector3 InitialPosition;
    private Vector3 EndPosition;
    public  Vector3 InitialDelta;
    public float delay = 0.1f;
    public bool useIndexforDelay = false;

    /// <summary>
    /// At start initialise the object.
    /// </summary>
    private void Start()
    {
        if (!isInitialised)
        {
            Initialise();
        }
    }

    /// <summary>
    /// Set up the start position and end position of the animation.
    /// </summary>
    void Initialise()
    {
        if (useIndexforDelay)
        {
            delay = 0.05f * transform.GetSiblingIndex();
        }
        EndPosition = GetComponent<RectTransform>().anchoredPosition;
        InitialPosition = EndPosition + InitialDelta;
        isInitialised = true;
    }

    /// <summary>
    /// When enabled start playing the animation.
    /// </summary>
    private void OnEnable()
    {
        if (!isInitialised)
        {
            Initialise();
        }
        GetComponent<RectTransform>().anchoredPosition = InitialPosition;
        Invoke("PlayAnimation", 0.2f + delay);
    }

    /// <summary>
    /// Play the animation.
    /// </summary>
    public void PlayAnimation()
    {
        GetComponent<RectTransform>().DOAnchorPos(EndPosition, 0.5f).SetEase(Ease.OutBack);
    }
}
