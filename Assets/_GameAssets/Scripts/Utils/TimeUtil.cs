﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;

public class TimeUtil : MonoBehaviour
{
    // Converts Int value into a string we can show to user.
    public static string TimeString(int TotalSeconds)
    {
        int s = TotalSeconds % 60;
        int m = TotalSeconds / 60;
        int h = m / 60;
        int d = h / 24;
        h = h % 24;
        m = m % 60;

        if (d > 1)
        {
            return d.ToString() + "D " + h.ToString() + "H";
        }
        else
        {
            return ((h > 0) ? (h > 9 ? "" : "0") + h.ToString() + ":" : string.Empty) + (m > 9 ? "" : "0") + m.ToString() + ":" + (s > 9 ? "" : "0") + s.ToString();
        }
    }
}
