﻿using System.Collections.Generic;

public class GameDelegates 
{
    public delegate void OnSimulationStart();
    public delegate void OnPerformedOneMove();

    public delegate void OnSimulationComplete();
}
