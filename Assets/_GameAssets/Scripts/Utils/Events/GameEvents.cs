﻿public static class GameEvents
{
    public static GameDelegates.OnSimulationStart OnSimulationStart;
    public static GameDelegates.OnPerformedOneMove OnPerformedOneMove;

    public static GameDelegates.OnSimulationComplete OnSimulationComplete;
}
