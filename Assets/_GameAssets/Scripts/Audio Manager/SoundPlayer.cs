﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

public static class SoundPlayer
{
    static SoundManager Sm => SoundManager.instance;

    public static void StopAllSfx()
    {
        if (Sm == null) return;
        Sm.sfxSource.Stop();
    }

    public static void PlayClickSound()
    {
        if (Sm == null) return;

        if (SoundManager.IsSfx)
        {
            Sm.sfxSource.PlayOneShot(Sm.ButtonSound);
        }
    }

    public static void PlayLevelCompleteSound()
    {
        if (Sm == null) return;

        if (SoundManager.IsSfx)
        {
            Sm.sfxSource.PlayOneShot(Sm.LevelCompleteSound);
        }
    }

    public static void PlayDiskPickSound()
    {
        if (Sm == null) return;

        if (SoundManager.IsSfx)
        {
            Sm.sfxSource.PlayOneShot(Sm.DiskPickSound);
        }
    }

    public static void PlayDiskDropSound()
    {
        if (Sm == null) return;

        if (SoundManager.IsSfx)
        {
            Sm.sfxSource.PlayOneShot(Sm.DiskDropSound);
        }
    }
}
