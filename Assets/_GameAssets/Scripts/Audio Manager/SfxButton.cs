﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SfxButton : MonoBehaviour
{
    [SerializeField] Sprite SfxOn, SfxOff;

    private void OnEnable()
    {
        SetUp();
    }

    public void OnButtonClick()
    {
        SoundManager.IsSfx = !SoundManager.IsSfx;
        SoundPlayer.PlayClickSound();
        SetUp();
    }

    void SetUp()
    {
        GetComponent<Image>().sprite = (SoundManager.IsSfx ? SfxOn : SfxOff);
    }
}
