﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    public AudioSource sfxSource;
    public AudioClip ButtonSound, LevelCompleteSound, DiskPickSound, DiskDropSound;

    public static bool IsSfx
    {
        get
        {
            return PlayerPrefs.GetInt("sfx") == 0;
        }
        set
        {
            PlayerPrefs.SetInt("sfx", value ? 0 : 1);
            if (!value)
            {
                if(instance != null)
                    instance.sfxSource.Stop();
            }
        }
    }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
}
