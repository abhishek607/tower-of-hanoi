﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class keeps the Move and Time data about the current game session.
/// </summary>
public class SessionDataController : MonoBehaviour
{
    public static SessionDataController instance;
    public int Moves;
    public int CurrentTime;

    private void Start()
    {
        instance = this;
    }

    private void OnEnable()
    {
        GameEvents.OnSimulationStart += OnSimulationStart;
    }

    private void OnDisable()
    {
        GameEvents.OnSimulationStart -= OnSimulationStart;
    }

    // At the start of a new simulation reset all the data.
    void OnSimulationStart()
    {
        Moves = 0;
        CurrentTime = 0;
    }
}
