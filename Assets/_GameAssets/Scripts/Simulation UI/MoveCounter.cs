﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Shows the move count on screen.
/// </summary>
public class MoveCounter : MonoBehaviour
{
    [SerializeField] Text MoveText;

    private void OnEnable()
    {
        MoveText.text = "Moves: 0";
        GameEvents.OnPerformedOneMove += OnPerformedOneMove;
    }

    private void OnDisable()
    {
        GameEvents.OnPerformedOneMove -= OnPerformedOneMove;
    }

    void OnPerformedOneMove()
    {
        MoveText.text = "Moves: " + SessionDataController.instance.Moves.ToString();
    }
}
