﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Shows the game's timer.
/// </summary>
public class TimerCounter : MonoBehaviour
{
    [SerializeField] Text TimerText;

    private void OnEnable()
    {
        TimerText.text = "00:00";
        StartCoroutine(TimerRoutine());
    }

    IEnumerator TimerRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (!GameHelper.IsPaused)
            {
                SessionDataController.instance.CurrentTime++;
                TimerText.text = TimeUtil.TimeString(SessionDataController.instance.CurrentTime);
            }
        }
    }
}
